package Tapalaga.Adrian.lab3.ex2;

public class Circle {
    private String color;
    private double radius;

    public Circle() {
        this.color = "red";
        this.radius = 1.0;
    }

    public Circle(double r1) {
        radius = r1;


    }

    public Circle(String c1) {
        color = c1;

    }

    public double getRadius() {
        return radius;

    }

    public double getArea() {
        return Math.PI * (radius * radius);
    }

}

