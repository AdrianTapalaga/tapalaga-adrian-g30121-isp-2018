package Tapalaga.Adrian.lab3.ex3;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender) {
        if (Character.toLowerCase(gender) == 'm' || Character.toLowerCase(gender) == 'f') {
            this.gender = gender;
        } else
            System.out.println("Letter is not accepted");
        this.name = name;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public char getGender() {
        return gender;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        System.out.println(getName() + "(" + getGender() + ") at " + getEmail());
    }
}
