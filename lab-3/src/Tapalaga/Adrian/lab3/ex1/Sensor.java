package Tapalaga.Adrian.lab3.ex1;

public class Sensor {
    private int value;

    public Sensor() {
        this.value = -1;
    }

    public void change(int k) {
        value += k;
    }

    public  int get() {
        return value;
    }
}
