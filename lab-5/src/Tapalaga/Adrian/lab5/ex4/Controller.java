package Tapalaga.Adrian.lab5.ex4;

public class Controller {
    private static Controller c1;

    private Controller() {
    }

    public static Controller getInstance() {
        if (c1 == null) {
            c1 = new Controller();
        }
        return c1;
    }
}

