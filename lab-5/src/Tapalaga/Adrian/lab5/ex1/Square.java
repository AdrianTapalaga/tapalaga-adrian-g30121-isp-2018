package Tapalaga.Adrian.lab5.ex1;

public class Square extends Rectangle {
    public Square() {

    }

    public Square(double side) {
        super(side, side);
        side=side;

    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);
    }

    public void getSide() {
        super.getWidth();
    }

    public void setSide(double side) {
         super.setLength(side);
         super.setWidth(side);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
    }

    @Override
    public String toString() {
        return "Square{}";
    }
}
