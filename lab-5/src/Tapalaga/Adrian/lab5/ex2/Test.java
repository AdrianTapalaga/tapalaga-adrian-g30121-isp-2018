package Tapalaga.Adrian.lab5.ex2;

public class Test {
    public static void main(String[] args) {
        RealImage r = new RealImage("Imagine1");
        r.display();
//        ProxyImage pi = new ProxyImage("Car cu boi");
//        pi.display();
        RotatedImage roti = new RotatedImage("Imegine2");
        roti.display();

        ProxyImage pi1 = new ProxyImage("Imagine",1);
        pi1.display();
        ProxyImage pi2 = new ProxyImage("Imagine",2);
        pi2.display();

    }
}
