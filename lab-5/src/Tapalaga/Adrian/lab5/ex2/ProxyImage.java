package Tapalaga.Adrian.lab5.ex2;

public class ProxyImage implements Image {
    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private int proxy;

    public ProxyImage(String fileName, int proxy){
        this.fileName = fileName;
        this.proxy=proxy;
    }

    @Override
    public void display() {
        if(proxy==1) {
            if (realImage == null) {
                realImage = new RealImage(fileName);
            }
            realImage.display();
        }
        if(proxy==2) {
            if (rotatedImage == null) {
                rotatedImage = new RotatedImage(fileName);
            }
            rotatedImage.display();
        }
    }
}
