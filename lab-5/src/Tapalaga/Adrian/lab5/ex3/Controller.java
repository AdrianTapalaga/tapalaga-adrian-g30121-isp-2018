package Tapalaga.Adrian.lab5.ex3;

public class Controller {
    public void control() throws InterruptedException {


        Sensor s = new TemperatureSensor();
        Sensor l = new LightSensor();

        int sec = 1;
        while (sec <=20)
        {
            System.out.println("Temp: " + s.readValue());
            System.out.println("Light: " + l.readValue());
            System.out.println("Sec: " + sec);
            sec++;
            Thread.sleep(1000);
        }

}
