package Tapalaga.Adrian.lab6.ex2;

public class Test {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Andi",100);
        bank.addAccount("Rares",90);
        bank.addAccount("Cosmina",80);
        bank.addAccount("Mihai",70);
        bank.addAccount("Claudia",60);
        bank.addAccount("Andreea",50);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between limits");
        bank.printAccounts(60,90);
        System.out.println("Get Account by owner name");
        bank.getAccount("Rares");
        System.out.println("Get All Account order by name");
        bank.getAllAccount();

    }
}
