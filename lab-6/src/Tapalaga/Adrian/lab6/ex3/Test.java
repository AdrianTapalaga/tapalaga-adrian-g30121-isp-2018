package Tapalaga.Adrian.lab6.ex3;

public class Test {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Andreea",105);
        bank.addAccount("Rares",90);
        bank.addAccount("Cosmina",180);
        bank.addAccount("Mihai",75);
        bank.addAccount("Claudiu",60);
        bank.addAccount("Andi",58);

        System.out.println("Print Accounts by balance");
        bank.printAccounts();

        System.out.println("Print Accounts between limits");
        bank.printAccounts(90,180);

        System.out.println("Get Account by owner name and balance");
        bank.getAccount("Radu",60);

        System.out.println("Get All Account ");
        bank.getAllAccount();

    }
}
