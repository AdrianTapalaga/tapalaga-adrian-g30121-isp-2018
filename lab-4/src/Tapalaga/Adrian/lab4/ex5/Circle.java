package Tapalaga.Adrian.lab4.ex5;

public class Circle {
    double radius;
    private String color;

    public Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(String color) {
        this.color = color;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return this.radius*this.radius*Math.PI;
    }
}
