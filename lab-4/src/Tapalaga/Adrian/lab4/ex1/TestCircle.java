package Tapalaga.Adrian.lab4.ex1;

public class TestCircle {
    public static void main(String[] args) {

        Circle c2 = new Circle("green");
        Circle c3 = new Circle(2.0);
        System.out.println(c2.getArea());
        System.out.println(c3.getRadius());
    }
}
