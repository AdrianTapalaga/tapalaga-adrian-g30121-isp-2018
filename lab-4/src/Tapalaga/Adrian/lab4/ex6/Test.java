package Tapalaga.Adrian.lab4.ex6;

public class Test {
    public static void main (String[] args) {
        Shape myShape = new Shape ();
        System.out.println (myShape.toString ());
        Circle myCircle = new Circle (3);
        System.out.println (myCircle.toString ());
        Rectangle myRectangle = new Rectangle (2,5);
        System.out.println (myRectangle.toString ());
        Square mySquare = new Square ();
        System.out.println (mySquare.toString ());
    }
}
