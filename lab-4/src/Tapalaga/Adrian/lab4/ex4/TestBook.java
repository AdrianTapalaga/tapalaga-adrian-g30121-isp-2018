package Tapalaga.Adrian.lab4.ex4;

import Tapalaga.Adrian.lab4.ex2.Author;
import java.util.Scanner;

public class TestBook {
    public static void main (String[] args) {

        Author[] myAuthor = new Author[25];
        Scanner in = new Scanner (System.in);
        String name = null;
        String email;
        char gender = 'm';
        for (int i = 0; i < 5; i++) {
            name = in.next ();
            email = in.next ();
            myAuthor[i] = new Author (name, email, gender);
        }
        for (int i = 0; i < 5; i++) {
            System.out.println (myAuthor[i].toString ());
        }

        Book myBook = new Book ("Test",myAuthor,19.99,99);
        System.out.println (myBook.toString ());


    }

}
