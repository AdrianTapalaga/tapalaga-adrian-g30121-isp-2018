package Tapalaga.Adrian.lab7.ex3;

import java.io.*;
import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;


public class FileEncrypt {
    File file;
    PrintWriter print;

    public void encrypt() throws Exception {
        try {
            WriteInFile b = new WriteInFile();
            b.write();
            file = new File("data2.txt");
            Scanner readfile = new Scanner(file);
            print = new PrintWriter("data.enc");
            String line = readfile.nextLine();
            char p = ' ';
            for (int i = 0; i < line.length(); i++) {
                p = (char) (line.charAt(i) << 1);
                print.print(p);
            }

        } catch (Exception e) {
            System.err.println("Error");
        }

    }

    public void decrypt() throws  Exception{
        try{
            file=new File("data.enc");
            print=new PrintWriter("data.dec");
            Scanner s=new Scanner(file);
            System.out.println("");
            String line=s.nextLine();
            char s1=' ';
            for(int i=0;i<line.length();i++){
                s1=(char)(line.charAt(i)>>1);
                print.print(s1);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
