package Tapalaga.Adrian.lab2.ex5;

import java.util.Random;


public class BubbleSort {
    public static void main(String args[]) {

        int n = 10;
        int v[] = new int[10];
        Random rand = new Random();
        for (int i = 0; i < v.length; i++) {
            v[i] = rand.nextInt(20);
        }

        System.out.println("Vectorul nesortat:");
        for (int i = 0; i < v.length; i++) {
            System.out.print(v[i] + " ");
        }
        System.out.println();

        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (v[j] > v[j + 1]) {
                    // swap temp and arr[i]
                    int temp = v[j];
                    v[j] = v[j + 1];
                    v[j + 1] = temp;
                }

        System.out.println("Vextorul sortat:");
        for (int i = 0; i < v.length; i++) {
            System.out.print(v[i] + " ");
        }
    }


}
