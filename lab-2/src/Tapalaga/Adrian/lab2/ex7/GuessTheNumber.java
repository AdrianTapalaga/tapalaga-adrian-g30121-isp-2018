package Tapalaga.Adrian.lab2.ex7;

import java.util.Random;
import java.util.Scanner;

public class GuessTheNumber {
    public static void main(String[] args) {
        int N;
        Random rand = new Random();
        N = rand.nextInt(20);
        Scanner in = new Scanner(System.in);
        System.out.println(N);
        int count = 3;
        while (count != 0) {
            int A = in.nextInt();

            if (A > N) {
                System.out.println("Wrong answer, your number it too high");
                count--;
            } else if (A < N) {
                System.out.println("Wrong answer, your number it too low");
                count--;
            } else if (A == N) {
                System.out.println("You win");
                break;
            }
        }
        if(count==0)
          System.out.println("You lost");
    }
}


