package Tapalaga.Adrian.lab2.ex6;

import java.util.Scanner;

public class Factoriale {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int p = 1;
        if (N == 0)
            p = 1;
        else {
            for (int i = 1; i <= N; i++) {
                p=p*i;
            }
        }
        System.out.println("Factorialul este "+p);

    }
}

