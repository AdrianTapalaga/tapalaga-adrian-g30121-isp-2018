package Tapalaga.Adrian.lab2.ex6;

import java.util.Scanner;



public class FactorialeRecursiv {


    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        int fact=1;
        fact = factorialRecursiv(N);
        System.out.println("Factorialul numarului" + N + " este: " + fact);
    }

    static int factorialRecursiv(int n) {
        if (n == 0)
            return 1;
        else
            return (n * factorialRecursiv(n - 1));


    }

}


