package Tapalaga.Adrian.lab2.ex4;

import java.util.Scanner;
import java.util.*;
import java.util.Collections;
import java.util.Vector;

public class MaxVector {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int max = -100000000;
        int N = in.nextInt();
        int[] v = new int[N];
        Random rand = new Random();
        for (int i = 0; i < v.length; i++) {
            v[i] = rand.nextInt(N);
        }

        for(int i=0;i<v.length;i++){
            System.out.print("v["+i+"]="+v[i]+" ");

        }

        for (int i = 0; i < v.length; i++) {
            if (v[i]>max) {
                max = v[i];
            }
        }

        System.out.println("Elementul maxim este "+max);

    }

}

