package Tapalaga.Adrian.lab2.ex3;

import java.util.Scanner;

public class NrPrime {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int count = 0;
        int A = in.nextInt();
        int B = in.nextInt();
        for (int number = A; number < B; number++) {
            if (isPrim(number)) {
                count++;
            }
        }
        System.out.println(count);

    }



    public static boolean isPrim(int nr) {
        for (int i = 2; i < nr / 2; i++) {
            if (nr % i == 0) {
                return false;
            }
        }
        return true;
    }
}
